MNIST
28x28
citirea csv
primul e labelul

scoatem x si y pt train si test
train - 60 000 de linii x 28x28+1 coloane

normalizare

1. invatati cifrele 0 si 1
functie de filtrare_subset din total
one hot - asociaza unui label un vector cu 0 pe poz i daca i != label si 1 altfel
pt diferenta dintre predictie si eticheta reala

functie sigmoida
functia de cost

de ce facem regularizare:
a complete tutorial on ridge and lasso regression in python

functie de update pt theta

procent de corectitudine peste 80-85

matricea de confuzie = pe setul de test
sist prezice 0 si label e 0 = true negative [0,0]
sist prezice 1 si label e 1 = true positive [1,1]
sist prezice 0 si label e 1 = false negative [1,0]
sist prezice 1 si label e 0 = false positive [0,1]
sub 20 pe diag sec

adunate sa dea nr de linii


apoi pt toate cifrele

functie care sa afiseze imaginea citita dintr-o linie din csv
imshow din matplotlib 
sa afisam si vectorul de one hot

random cu randn

100 de epoci (adica iteratii) 15 minute
matrice de 3 Gb pt double ???


OPtional:
impartim setul de train in k parti random
antrenam cu k-1
calculam costul pe ultima parte, nu pe cele k-1
repeta de k ori  (reimpartim random setul)
suma costurilor obtinute
trebuie facuta pt fiecare pereche de alpha si lambda
pt alpha si lambda

